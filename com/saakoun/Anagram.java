package com.saakoun;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Anagram {

	public static void main(String[] args) {
		Anagram anagram = new Anagram();
		if (args.length > 0) {
			anagram.findAnagrams(args[0]);
		} else {
			System.out.println("Unable to find file path");
		}
	}

	void findAnagrams(String filename) {
		long startTime = System.nanoTime();
		String line = null;
		Map<String, List<String>> sortedWordMap = new HashMap<>();
		Map<String, Integer> rowsMap = new HashMap<>();
		Path path = Paths.get(filename);
		int row = 1;
		try (BufferedReader reader = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {
			while ((line = reader.readLine()) != null) {
				// skip duplicates
				if (!rowsMap.containsKey(line)) {
					String sortedString = sortString(line);
					if (sortedWordMap.containsKey(sortedString)) {
						sortedWordMap.get(sortedString).add(line);
						rowsMap.put(line, row);
					} else {
						List<String> list = new ArrayList<>();
						list.add(line);
						sortedWordMap.put(sortedString, list);
						rowsMap.put(line, row);
					}
				}
				row++;
			}
		} catch (IOException e) {
			System.out.println("Unable to open file '" + path.getFileName() + "'");
		}
		for (Entry<String, List<String>> entry : sortedWordMap.entrySet()) {
			if (entry.getValue().size() > 1) {
				System.out.println("Anagram groups");
				System.out.println("*********************************");
				Object[] arr = entry.getValue().toArray();
				for (Object word : arr) {
					System.out.println(word.toString() + " => " + rowsMap.get(word));

				}
				System.out.println("*********************************");
			}
		}
		System.out.println("Algorithme de complexité linéaire o(2n), n file max row number");
		System.out.format("The process took: %f seconds", (double) (System.nanoTime() - startTime) / 1000000000.0);

	}

	private static String sortString(String s) {
		char[] a = s.toCharArray();
		Arrays.sort(a);
		return new String(a).toLowerCase();
	}

}
