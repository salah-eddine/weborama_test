package com.saakoun;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class Doublon {

	public static void main(String[] args) {
		Doublon doublon = new Doublon();
		if (args.length > 0) {
			doublon.findDuplicates(args[0]);
		} else {
			System.out.println("Unable to find file path");
		}
	}

	void findDuplicates(String filename) {
		long startTime = System.nanoTime();
		String line = null;
		Map<String, Integer> mapLineCounter = new HashMap<>();
		Map<String, Integer> mapRowSaver = new HashMap<>();
		Path path = Paths.get(filename);
		int row = 1;
		try (BufferedReader reader = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {
			while ((line = reader.readLine()) != null) {
				if (mapLineCounter.containsKey(line)) {
					System.out.println("Doublon => " + line + " {" + mapRowSaver.get(line) + "," + row + "}");
				} else {
					mapLineCounter.put(line, 1);
					mapRowSaver.put(line, row);
				}
				row++;
			}
		} catch (IOException e) {
			System.out.println("Unable to open file '" + path.getFileName() + "'");
		}
		System.out.println("Algorithme de complexité linéaire o(n), n file max row number");
		System.out.format("The process took: %f seconds", (double) (System.nanoTime() - startTime) / 1000000000.0);
	}

}
