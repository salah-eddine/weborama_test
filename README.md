### weborama_test

Running this test requires jdk 1.7 minimum 

## Getting started

- Get the project :

```shell
$ git clone https://salah-eddine@bitbucket.org/salah-eddine/weborama_test.git
```

## Running Exercice 1

```sh
$ cd weborama_test
$ javac com/saakoun/Doublon.java
$ java -classpath . com.saakoun.Doublon PATH_LOG_FILE/fichier-ID.log
```

# Output example

Doublon => 1x12@Fu38Xv1 {898,899}
Doublon => f1RiZgi0nrQj {1345,1666}
Doublon => zhsOmCHJSgbz {6465,8002}
Algorithme de complexité linéaire o(n), n file max row number
The process took: 1,158691 seconds

## Running Exercice 2

```sh
$ cd weborama_test
$ javac com/saakoun/Anagram.java
$ java -classpath . com.saakoun.Anagram PATH_LOG_FILE/fichier-ID.log
```

# Output example

Anagram groups
*********************************
keep => 1000001
peek => 1000002
*********************************
Anagram groups
*********************************
jMWU60DQVvjy => 8963
MjWy06DQVvjU => 8964
*********************************
Algorithme de complexité linéaire o(2n), n file max row number
The process took: 2,208010 seconds